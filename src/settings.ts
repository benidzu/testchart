/*
 *  Power BI Visualizations
 *
 *  Copyright (c) Microsoft Corporation
 *  All rights reserved.
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the ""Software""), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

module powerbi.extensibility.visual {
    "use strict";
    import DataViewObjectsParser = powerbi.extensibility.utils.dataview.DataViewObjectsParser;

    export class VisualSettings extends DataViewObjectsParser {
      public labels: LabelSettings = new LabelSettings();
      public legend: LegendSettings = new LegendSettings();
      public colorSchemes: ColorSchemes = new ColorSchemes();
      public borderOfData: BorderOfDataPoints = new BorderOfDataPoints();
      public tooltips: Tooltips = new Tooltips();
    }

    export class LabelSettings {
      public axislabels: boolean = true;
      public datalabels: boolean = false;
      public gridlines: boolean = false;
      public average: boolean = false;
    }

    export class LegendSettings {
      public show: boolean = true;
      public position: Positions = Positions.Top;
    }

    export enum Positions{
      Right = 0,
      Top = 1
    }

    export enum Palletes{
      Pallette0 = 0,
      Pallette1 = 1,
      Pallette2 = 2,
      Pallette3 = 3,
      Pallette4 = 4
    }

    export class ColorSchemes{
      public ordinaryPalletes: Palletes = Palletes.Pallette0;
      public sequentialSchemes: boolean = false;
      public color: string = "#424242";
    }
    
    export class BorderOfDataPoints{
      public show: boolean = true;
      public color: string = "#000000";
    }

    export class Tooltips{
      public show: boolean = true;
    }

}
