import tooltip = powerbi.extensibility.utils.tooltip;

let pickr = (<any>window).Pickr;

module powerbi.extensibility.visual {
    "use strict";

    export class Visual implements IVisual {
        private visualSettings: VisualSettings;
        private div: d3.Selection<any>;
        private svg: d3.Selection<any>;
        private visualHost: IVisualHost;
        private locale: string;
        private tooltipServiceWrapper: tooltip.ITooltipServiceWrapper;
        private colorScheme: ColorSchemes;


        constructor(options: VisualConstructorOptions) {
            //console.log('Visual constructor', options);
            this.div = d3.select(options.element)
                .append("div").attr("id","maindiv");
            this.svg = d3.select(options.element)
                .append("svg").attr("id","mainsvg");

            let self = this;
            this.svg.on("mouseover",function(){
                self.div.attr("style","display:block")
            })
            this.svg.on("mouseout",function(){
                self.div.attr("style","display:none")
            })
            this.div.on("mouseover",function(){
                self.div.attr("style","display:block")
            })
            this.div.on("mouseout",function(){
                self.div.attr("style","display:none")
            })

            this.locale = options.host.locale;
            this.visualHost = options.host;
            
            this.tooltipServiceWrapper = tooltip.createTooltipServiceWrapper(options.host.tooltipService, options.element);

            this.colorScheme = new ColorSchemes();
            this.colorScheme.init();
        }

        private getTooltipData(value: any, options: VisualUpdateOptions): VisualTooltipDataItem[] {
            let sf = d3.format(".2f");

            let dataSegments: VisualTooltipDataItem[];
            dataSegments = [];

            dataSegments.push( {
                displayName: options.dataViews[0].categorical.values[0].source.displayName,
                value: sf(d3.formatPrefix(value.xval).scale(value.xval)).toString()+d3.formatPrefix(value.xval).symbol,
                color: value.color,
                header: 'Details'
            });
            dataSegments.push(
            {
                displayName: options.dataViews[0].categorical.values[1].source.displayName,
                value: sf(d3.formatPrefix(value.yval).scale(value.yval)).toString()+d3.formatPrefix(value.yval).symbol,
                color: value.color,
            });  

            if(value.colorn != ""){
                dataSegments.push( {
                    displayName: this.getColorDimension(options).source.displayName,
                    value: value.colorn,
                    color: value.color,
                });
            }

            if(value.category != ""){
                dataSegments.push( {
                    displayName: this.getCategoricalDimension(options).source.displayName,
                    value: value.category,
                    color: value.color,
                });
            }
           return dataSegments;
        }

        private colorize(value: any, options: VisualUpdateOptions, colorScale): VisualTooltipDataItem[] {

            this.svg.selectAll("circle").attr({
                fill: d => colorScale(d.colorn),
                opacity: 0.75
            });

            let circles = this.svg.selectAll("circle").filter(function(d){
                return (d.xval) == (value.xval) && (d.yval) == (value.yval)
            });
            
            circles.attr({
                fill: d => colorScale(d.colorn),
                opacity: 1
            });
           
            return null;
        }


        public enumerateObjectInstances(options: EnumerateVisualObjectInstancesOptions): VisualObjectInstanceEnumeration {
            const settings: VisualSettings = this.visualSettings ||
                VisualSettings.getDefault() as VisualSettings;
            return VisualSettings.enumerateObjectInstances(settings, options);
        }
        //Ob spremembi na grafu se klice update funkcija
        public update(options: VisualUpdateOptions) {
            
            this.visualSettings = VisualSettings.parse<VisualSettings>(options.dataViews[0]);

            this.svg.attr({
                width: Math.round(options.viewport.width),
                height: Math.round(options.viewport.height),
            });

            let dataPoints = this.getDataPoints(options);
            this.plotScatterChart(dataPoints, options.viewport.width, options.viewport.height, this.visualSettings, options);
        }

        private getDataPoints(options: VisualUpdateOptions): DataPoint[] {
            let dataView = options.dataViews[0];
            let xval = dataView.categorical.values[0];
            let yval = dataView.categorical.values[1];


            let categories = this.getCategoricalDimension(options);
            let colors = this.getColorDimension(options);

            if (!dataView) {
                return;
            }
            if (yval == undefined)
                return;
            let dataPoints: DataPoint[] = [];
            xval.values.map((value, index) => {
                let label = helpers.getDataLabel(<number>value, 2, "Auto", this.locale, "");
                dataPoints.push({
                    xval: <number>value,
                    yval: <number>yval.values[index],
                    category: <string>(categories ? categories.values[index] : ""),
                    label: label,
                    colorn: <string>(colors ? colors.values[index] : ""),
                    // Getting the width of the label, based on font type, size, font weight
                    labelWidth: helpers.measureTextWidth(label, 12, "Segoe UI", "normal")
                });
            });
            return dataPoints;
        }

        private getCategoricalDimension(options: VisualUpdateOptions) {
            //returns data set with role "details", if it exists
            let cats = options.dataViews[0].categorical.categories;
            if (cats) {
                for (let element of cats)
                    if (element.source.roles.category)
                        return element;
            }
            else return undefined;
        }

        private getColorDimension(options: VisualUpdateOptions) {
            //returns data set with role "barve", if it exists
            let cats = options.dataViews[0].categorical.categories;
            if (cats) {
                for (let element of cats)
                    if (element.source.roles.color)
                        return element;
            }
            else return undefined;
        }

        private drawAverageLines(xvalues: number[],yvalues: number[],xScale: d3.scale.Linear<number, number>,yScale: d3.scale.Linear<number, number>){
            let avgx = 0;
            let avgy = 0;
            xvalues.forEach(val=>avgx+=val);
            yvalues.forEach(val=>avgy+=val);
            avgx=avgx/xvalues.length;
            avgy=avgy/yvalues.length;

            

            let grp = this.svg.append("g");

            let xAvgAxis = d3.svg.axis().scale(xScale).orient("bottom")
                .outerTickSize(0)
                .ticks(0);
            let yAvgAxis = d3.svg.axis().scale(yScale).orient("left")
                .ticks(0)
                .outerTickSize(0);

            let xAvgAxisG = grp.append("g")
            .attr("transform", "translate(0," + (yScale(avgy)) + ")")
            .attr("class", "x avg");
            let yAvgAxisG = grp.append("g")
            .attr("transform", "translate(" + ( xScale(avgx) )+ ",0)")
            .attr("class", "y avg");

            xAvgAxisG.call(xAvgAxis);
            yAvgAxisG.call(yAvgAxis);

        }

        //Option: border
        private border(visualsettings: VisualSettings){
            if(visualsettings.borderOfData.show){
                return visualsettings.borderOfData.color;
            }
            return undefined;
        }

        private drawGridLines(xScale: d3.scale.Linear<number, number>,yScale: d3.scale.Linear<number, number>, margin: { left: any; top?: number; right?: number; bottom?: number; }, innerHeight: number, innerWidth: number){

            let grp = this.svg.append("g");
            
            let xGridAxis = d3.svg.axis().scale(xScale).orient("bottom")
                .tickSize(-innerHeight)
                .tickFormat("")
                .ticks(5);
            let yGridAxis = d3.svg.axis().scale(yScale).orient("left")
                .ticks(5)
                .tickFormat("")
                .tickSize(-innerWidth);

            let xGridAxisG = grp.append("g")
            .attr("transform", "translate(0," + (innerHeight+margin.top) + ")")
            .attr("class", "x grid");
            let yGridAxisG = grp.append("g")
            .attr("transform", "translate(" + margin.left + ",0)")
            .attr("class", "y grid");

            xGridAxisG.call(xGridAxis);
            yGridAxisG.call(yGridAxis);
        }

        private calculateLegendMargins(visualsettings: VisualSettings,options: VisualUpdateOptions ){
            let obj = {top:0, right:0}
            if (visualsettings.legend.show && this.getColorDimension(options)!= undefined){
                if (visualsettings.legend.position == 0){
                    obj.right = 130;
                } else {
                    obj.top = 25;
                }
            }
            return obj;
        }

        private drawLegend(visualsettings: VisualSettings,margin,options:VisualUpdateOptions,colorScale,innerWidth){
            let sidebarLeftMargin = 0;
            let drawright = visualsettings.legend.position == 0;

            if (visualsettings.legend.position == 0)
                sidebarLeftMargin = margin.left + innerWidth + 5;

            let sidebar = this.svg.append("g")
                .attr("transform", "translate(" + sidebarLeftMargin + "," + ( drawright ? margin.top+10 : 3*margin.top/4 ) + ")")
                .attr("class", "sidebar");
            
            let cats = this.getColorDimension(options);
            let LegendTitle = (cats == undefined) ? "" : cats.source.displayName;

            
            //Draw legend
            let colorLegend = sidebar.append("g")
            colorLegend.append("text")
                .text(LegendTitle)
                .attr("y", drawright? "5" : "0")
                .attr("x", drawright? "10" : "0")
                .attr("class", "categorylabel")

            let colorLegendDataYoffset = drawright? 20 : 0;
            let colorLegendDataXoffset = drawright? 0 : 0;

            let colorLegendData = colorLegend.append("g")
                .attr("transform", "translate("+colorLegendDataXoffset +"," + colorLegendDataYoffset + ")");

            let acc = helpers.measureTextWidth(LegendTitle, 12, "sans-serif", "normal")+4;
            colorScale.domain().forEach((element: d3.Primitive,ix: number) => {
                colorLegendData.append("rect")
                .attr("x",drawright? 6 : 1.3*acc)
                .attr("y",drawright? ix*30 : -13)
                .attr("height",16)
                .attr("width",16)
                .attr("opacity",0.75)
                .attr("fill",colorScale(element));
                
                colorLegendData.append("text")
                .attr("y",drawright? 13+ix*30 : 0)
                .attr("x",drawright? 30 : 1.3*acc+17)
                .attr("class","categorydata")
                .text(element)

                acc+= helpers.measureTextWidth(element.toString(), 11, "sans-serif", "normal")+18;
            });   
        }

        private drawBasicUI(visualsettings: VisualSettings,margin,options:VisualUpdateOptions,innerWidth){
            let self = this;
            let showavg = visualsettings.labels.average;

            let UIdiv = this.div.append("div")
                .style("right","0px")
                .attr("class", "uidiv");
            
            let firstsvg = UIdiv.append("svg")
            .attr("transform", "translate(+"+ 0 +"" + 0 + ")")
            .attr("height", 40)
            .attr("width", 70)


            //average lines
            let avl = firstsvg.append("foreignObject")
           .attr("width",20)
           .attr("height",20)
           .attr("x",0)
           avl.append("xhtml:div")
           .attr("class", "fo1")
           .attr("id",showavg? "l1" : "l2")
           .text("A")

           avl.on("click",function(){
                self.visualHost.persistProperties({
                    merge: 
                        [
                            {
                            objectName: "labels",
                            selector: null,
                            properties: {
                                average: showavg ? false : true
                                }
                            }
                        ]
                });
            })

            let showgl = visualsettings.labels.gridlines;
            //grid lines
            let gl = firstsvg.append("foreignObject")
           .attr("width",20)
           .attr("height",20)
           .attr("x",26)
           gl.append("xhtml:div")
           .attr("class", "fo1")
           .attr("id",showgl? "l1" : "l2")
           .text("G")

           gl.on("click",function(){
                self.visualHost.persistProperties({
                    merge: 
                        [
                            {
                            objectName: "labels",
                            selector: null,
                            properties: {
                                gridlines: showgl ? false : true
                                }
                            }
                        ]
                });
            })
        }

        private drawUI(visualsettings: VisualSettings,margin,options:VisualUpdateOptions,innerWidth){

            let isSequential = visualsettings.colorSchemes.sequentialSchemes;
            let showleg = visualsettings.legend.show;
            let self = this;

            this.div.selectAll("div").remove();
            let UIdiv = this.div.append("div")
                .attr("class", "uidiv");


            //svg seznam UI elementov
            let firstsvg = UIdiv.append("svg")
            .attr("x", 0 )
            .attr("y", 10)
            .attr("height",300)
            .attr("width",300)
            
            //dodaj stikalo za spremembo kategoricnih barvnih shem
            if (!isSequential){
                let fgn = firstsvg.append("foreignObject")
                .attr("width",20)
                .attr("height",20)
                .attr("x",30)
                fgn.append("xhtml:div")
                .attr("class","fo2" )
                .attr("id","d"+visualsettings.colorSchemes.ordinaryPalletes)
                .text( visualsettings.colorSchemes.ordinaryPalletes)
                fgn.on("click",function(){
                    self.visualHost.persistProperties({
                        merge: 
                            [
                                {
                                objectName: "colorSchemes",
                                selector: null,
                                properties: {
                                    ordinaryPalletes: (visualsettings.colorSchemes.ordinaryPalletes+1) % 5
                                    }
                                }
                            ]
                    });
                })
            }

            
            //color picker switch
            if (isSequential){
                let fgn = firstsvg.append("foreignObject")
                .attr("width",300)
                .attr("height",300)
                .attr("x",30)
                fgn.append("xhtml:div")
                .attr("class","fo3" )
                .attr("width",20)
                .attr("style","float:left")
                .attr("height",20)
                .attr("id","color-picker-container-btn")

                //init Pickr color picker
                const colorpkr = pickr.create({
                    el: '#color-picker-container-btn',
                    theme: 'monolith', 
                    
                    appClass: 'clrp',
                    swatches: null,
                    adjustableNumbers: true,
                    lockOpacity: true,
                    default: visualsettings.colorSchemes.color,
                    useAsButton: true,
                    autoReposition: false,

                    components: {                
                        // Main components
                        
                        preview: true,
                        hue: true,     
                        defaultRepresentation: 'RGBA',
                                                
                        // Input / output Options
                        interaction: {                           
                            rgba: true,
                            input: true,
                            save: true,
                        }
                    }
                });
                
                colorpkr.on("save", (color) => {
                    self.visualHost.persistProperties({
                        merge: 
                            [
                                {
                                objectName: "colorSchemes",
                                selector: null,
                                properties: {
                                    color: color.toHEXA().toString()
                                    }
                                }
                            ]
                    });
                })
                
                //premaknemo color-picker node na pravo mesto - po defaultu ga postavi izven vizualizacije
                let colorpickernode = d3.select(".pcr-app").node();
                fgn.node().appendChild(colorpickernode);
            }


           //stikalo za preklop med sekvencnimi/kategoricnimi barvami 
           let fo = firstsvg.append("foreignObject")
           .attr("width",20)
           .attr("height",20)
           .attr("x",5)

           fo.append("xhtml:div")
           .attr("class",isSequential? "fo1" : "fo2")
           .text(isSequential ? "S" : "C" )

            fo.on("click",function(){
                self.visualHost.persistProperties({
                    merge: 
                        [
                            {
                            objectName: "colorSchemes",
                            selector: null,
                            properties: {
                                sequentialSchemes: isSequential ? false : true
                                }
                            }
                        ]
                });
            })

           //stikalo za prikaz legende
           let leg = firstsvg.append("foreignObject")
           .attr("width",20)
           .attr("height",20)
           .attr("x",70)
           leg.append("xhtml:div")
           .attr("class", "fo1")
           .attr("id",showleg? "l1" : "l2")
           .text("L")

           leg.on("click",function(){
                self.visualHost.persistProperties({
                    merge: 
                        [
                            {
                            objectName: "legend",
                            selector: null,
                            properties: {
                                show: showleg ? false : true
                                }
                            }
                        ]
                });
            })


            //puscica za izbiro pozicije legende
            if (showleg){
                let fgn = firstsvg.append("g")
                .attr("width",20)
                .attr("height",20)
                .attr("transform", "translate(95," + 5 + ")")
                fgn.append("polygon")
                .attr("cursor","pointer")
                .attr("id","caret")
                .attr("points", visualsettings.legend.position == 1 ? "0,10 5,0 10,10" : "0,0 10,5 0,10")
                .attr("style", "stroke:black")

                fgn.on("click",function(){
                    self.visualHost.persistProperties({
                        merge: 
                            [
                                {
                                objectName: "legend",
                                selector: null,
                                properties: {
                                    position:  visualsettings.legend.position == 1 ? 0 : 1
                                    }
                                }
                            ]
                    });
                })
            }
        }

        private plotScatterChart(dataPoints: DataPoint[], width: number, height: number, visualsettings: VisualSettings, options: VisualUpdateOptions): any {
            
            let dataView = options.dataViews[0];
            let xvalues = dataPoints.map(d => d.xval);
            let yvalues = dataPoints.map(d => d.yval);
            
            let xmin = d3.min(xvalues);
            let xmax = d3.max(xvalues);
            let ymin = d3.min(yvalues);
            let ymax = d3.max(yvalues);

            //compute scale size
            let overhead = { x: 0.1, y: 0.1 };
            let ranges = { x: overhead.x * (xmax - xmin), y: overhead.y * (ymax - ymin) };
            xmin -= ranges.x;
            xmax += ranges.x;
            ymin -= ranges.y;
            ymax += ranges.y;

            let margin = { left: 60, top: 25, right: 25, bottom: 50 };
            if (!visualsettings.labels.axislabels) {
                margin.left = 45;
                margin.bottom = 25;
            }
            //let legendmargin = (visualsettings.legend.show && this.getColorDimension(options)!= undefined)? (function(){return 130}()) : 0;
            let legendmargin = this.calculateLegendMargins(visualsettings,options);
            let innerWidth = width - margin.left - margin.right - legendmargin.right;
            let innerHeight = height - margin.top - margin.bottom - legendmargin.top;
            
            margin.top += legendmargin.top;
            
        

            let xScale = d3.scale.linear().domain([xmin, xmax]).rangeRound([margin.left, margin.left + innerWidth]);
            let yScale = d3.scale.linear().domain([ymin, ymax]).rangeRound([innerHeight+margin.top, margin.top]);

            //Inicializacija barvnih shem
            let colorScale = null;
            if(visualsettings.colorSchemes.sequentialSchemes){
                this.colorScheme.generateSequentialColorScheme(visualsettings.colorSchemes.color);
                let seqColors = this.colorScheme.colorSchemesSequential.value;
                colorScale = d3.scale.ordinal().range(seqColors);
            }else{
                colorScale = d3.scale.ordinal().range(this.colorScheme.colorSchemesOrdinary[visualsettings.colorSchemes.ordinaryPalletes].value); 
            }

            // Plot the circles
            let circles = this.svg.selectAll("circle").data(dataPoints);
            circles.enter().append("circle");
            circles.exit().remove();
            circles.attr({
                fill: d => colorScale(d.colorn),
                stroke: this.border(visualsettings),
                opacity: 0.75,
                cx: d => Math.round(xScale(d.xval)),
                cy: d => Math.round(yScale(d.yval)),
                r: Math.min(width, height) * 0.025
            });

            //Option: tooltip
            if(this.visualSettings.tooltips.show){
                this.tooltipServiceWrapper.addTooltip(this.svg.selectAll('circle'),
                (tooltipEvent: tooltip.TooltipEventArgs<number>) => this.getTooltipData(tooltipEvent.data,options),
                (tooltipEvent: tooltip.TooltipEventArgs<number>) => this.colorize(tooltipEvent.data, options, colorScale)
                );
            }else{
                this.tooltipServiceWrapper.hide();
                //this.svg.selectAll('circle').remove();
            }
            
            // Add the labels
            let labels = this.svg.selectAll(".labels").data(dataPoints);
            labels.exit().remove();
            labels.enter().append("text").classed("labels", true);
            labels
                .text(d => visualsettings.labels.datalabels ? d.label : "")
                .attr({
                    x: d => Math.round(xScale(d.xval)),
                    y: d => Math.round(yScale(d.yval) - 15),
                })
                .style("font-family", "Segoe UI")
                .style("font-size", "12")
                .style("font-weight", "normal");


            //Remove old group elements
            this.svg.selectAll("g").remove();
            this.div.selectAll("input").remove();
            this.div.selectAll("div").remove();
            let g = this.svg.append("g");

            //Option: Draw legend
            if ((visualsettings.legend.show && this.getColorDimension(options)!= undefined)){
                this.drawLegend(visualsettings,margin,options,colorScale,innerWidth);
            }

            //draw category based UI
            if ( this.getColorDimension(options) != undefined)
                this.drawUI(visualsettings,margin,options,innerWidth);

            //draw basic UI options
            if (this.getColorDimension(options) != undefined || this.getCategoricalDimension(options) != undefined )
                this.drawBasicUI(visualsettings,margin,options,innerWidth);

            //Option: plot average lines
            if (visualsettings.labels.average && (this.getCategoricalDimension(options) != undefined || this.getColorDimension(options) != undefined)){
                this.drawAverageLines(xvalues,yvalues,xScale, yScale)
            }
            
            //Option: plot grid lines
            if (visualsettings.labels.gridlines && (this.getCategoricalDimension(options) != undefined || this.getColorDimension(options) != undefined)){
                this.drawGridLines(xScale, yScale,margin,innerHeight,innerWidth)
            }

            //Plot the axes
            let xAxisText = dataView.categorical.values[0].source.displayName;
            let xAxisLabelOffset = 40;
            let yAxisText = dataView.categorical.values[1].source.displayName;
            let yAxisLabelOffset = 45;

            let xAxisG = g.append("g")
                .attr("transform", "translate(0," + (innerHeight + margin.top) + ")")
                .attr("class", "x axis");
            let xAxisLabel = xAxisG.append("text")
                .style("text-anchor", "middle")
                .attr("x", innerWidth / 2 + margin.left)
                .attr("y", xAxisLabelOffset)
                .attr("class", "label")
                .text(visualsettings.labels.axislabels ? xAxisText : "");
            let yAxisG = g.append("g")
                .attr("transform", "translate(" + margin.left + ",0)")
                .attr("class", "y axis");
            let yAxisLabel = yAxisG.append("text")
                .style("text-anchor", "middle")
                .attr("transform", "translate(-" + yAxisLabelOffset + "," + innerHeight / 2 + ") rotate(-90)")
                .attr("class", "label")
                .text(visualsettings.labels.axislabels ? yAxisText : "");

            let xAxis = d3.svg.axis().scale(xScale).orient("bottom")
                .outerTickSize(0)
                .tickFormat(d3.format("s"))
                .ticks(5);
            let yAxis = d3.svg.axis().scale(yScale).orient("left")
                .ticks(5)
                .tickFormat(d3.format("s"))
                .outerTickSize(0);

            xAxisG.call(xAxis);
            yAxisG.call(yAxis);
            
            this.svg.attr("cursor","default");
        }   
    }

    interface DataPoint {
        xval: number;
        yval: number;
        label: string;
        category: string;
        labelWidth: number;
        colorn: string;
    }

   
    class ColorSchemes{
    
        public colorSchemesOrdinary: ColorScheme[]; 
        public colorSchemesSequential: ColorScheme;
        
        constructor(){
            this.colorSchemesOrdinary = [];
            this.colorSchemesSequential = null;
            return this;
        }
        /**
         * 
         * @param color 
         * Generiranje sekvencne barvne sheme na podlagi osrednje barve color
         */
        public generateSequentialColorScheme(color: string){

            let barve: d3.Hsl[];
            
            let koeficient = 0.25;     
            barve = [d3.hsl(color), d3.hsl(color), d3.hsl(color), d3.hsl(color), d3.hsl(color), d3.hsl(color), d3.hsl(color)];
            
            //Ce uporabnik vnese zelo svetlo ali zelo temno barvo, prilagodimo formulo.
            for(let i = 0; i < barve.length; i++){
                if(barve[i].l < 0.2){
                    barve[i].l = 0.2;
                }else if(barve[i].l > 0.8){
                    barve[i].l = 0.80;
                }
            }

            for(let i = 0; i< barve.length/2; i++){
                barve[i].l -= (3-i)*koeficient*barve[i].l;
            }
            for(let i = Math.floor(barve.length/2); i< barve.length; i++){
                barve[i].l += (i-3)*koeficient*barve[i].l;
            }
            
            let zaPretvorit: string[];
            zaPretvorit = [];

            for(let i = 0; i < barve.length; i++){
                let rgbColor = barve[i].rgb();
                zaPretvorit.push(rgbColor.toString());
            }
            this.colorSchemesSequential = {
                value: zaPretvorit
            };
        }
        
        //Inicializacija shem
        public init(){
            
            this.colorSchemesOrdinary.push(
                {
                    value: ["#c03221", "#0000ff", "#f2d0a4", "#545e75", "#3f826d", "#bbbe64", "#aac0aa"]
                }
            );
            this.colorSchemesOrdinary.push(
                {
                    value: ["#000000", "#25109a", "#ff0000", "#1e5a59", "#ffab12", "#39ca88", "b6dbb1"]
                }
            );
            this.colorSchemesOrdinary.push(
                {
                    value: ["#5e4b51", "#8dbeb2", "#f5e7c8", "#e2c787", "#dc886e"]
                }
            );
            this.colorSchemesOrdinary.push(
                {
                    value: ["#794c3b", "#e13933", "#fdfcfb", "#0f535a", "#0b1f24"]
                }
            );
            this.colorSchemesOrdinary.push(
                {
                    value: ["#c0631f", "#e3ac27", "#ddcda1", "#808c7c", "#373641"]
                }
            );
        }
    }
    
    interface ColorScheme{
        value: string[]
    }
}
